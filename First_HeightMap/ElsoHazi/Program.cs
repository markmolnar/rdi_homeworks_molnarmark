﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ElsoHazi
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                HeightMap hm = new HeightMap();
                hm.Parse(args[0]);
                hm.ElevationThreshold = Convert.ToInt32(args[2]);
                hm.SaveToBitmap(args[1]);
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine("Missing argument(s)!\nPropper form of the command:\nElsoHazi.exe inputFileName.hgt outputFileName elevationThreshold");
                Console.ReadKey();
            }
            catch (FormatException e)
            {
                Console.WriteLine("Not a number value for elevationThreshold!\nPropper form of the command:\nElsoHazi.exe inputFileName.hgt outputFileName elevationThreshold");
                Console.ReadKey();
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("Not an existing input file!\nPropper form of the command:\nElsoHazi.exe inputFileName.hgt outputFileName elevationThreshold");
                Console.ReadKey();
            }
        }
    }
}
