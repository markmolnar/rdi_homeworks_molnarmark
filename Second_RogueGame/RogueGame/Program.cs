﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Jatek.Keret;
using RogueGame.Jatek.Megjelenites;

namespace RogueGame.Jatek.Jatekter
{
    abstract class JatekElem
    {
        int x;
        int y;
        protected JatekTer ter;

        public abstract double Meret
        {
            get;
        }


        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
            }
        }
        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
            }
        }

        public JatekElem(int x,int y, JatekTer ter)
        {
            this.X = x;
            this.Y = y;
            this.ter = ter;
            this.ter.Felvetel(this);
        }

        public abstract void Utkozes(JatekElem elem);

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("(").Append(this.X).Append(",").Append(this.Y).Append(")");
            return builder.ToString();
        }
    }

    class JatekTer:IMegjelenitheto
    {
        //const int MAX_ELEMSZAM = 1000;
        //private int elemN;
        //private JatekElem[] elemek;
        List<JatekElem> elemek;
        int meretX;
        int meretY;

        public int MeretX
        {
            get
            {
                return meretX;
            }
            private set { }
        }
        public int MeretY
        {
            get
            {
                return meretY;
            }
            private set { }
        }

        public int[] MegjelenitendoMeret
        {
            get
            {
                int[] meretek=new int[2];
                meretek[0] = MeretX;
                meretek[1] = MeretY;
                return meretek;
            }
        }

        public JatekTer(int meretx,int merety)
        {
            this.meretX = meretx;
            this.meretY = merety;
            elemek = new List<JatekElem>();
        }

        public void Felvetel(JatekElem elem)
        {
            elemek.Add(elem);
            //elemN++;
        }

        public void Torles(JatekElem elem)
        {
            elemek.Remove(elem);
            //elemN--;
        }

        public List<JatekElem> MegadottHelyenLevok(int x, int y, int tavolsag)
        {
            List<JatekElem> tavolsagonBeluliElemek = new List<JatekElem>();
            foreach (JatekElem elem in elemek)
            {
                int distance =(int) Math.Sqrt(Math.Pow((double)(elem.X-x),(double) 2)+Math.Pow((double)(elem.Y-y), (double)2));
                //Console.WriteLine(distance);
                if (distance<=tavolsag)
                {
                    tavolsagonBeluliElemek.Add(elem);
                }
            }
            //Console.WriteLine(tavolsagonBeluliElemek.Count);
            return tavolsagonBeluliElemek;
        }

        public List<JatekElem> MegadottHelyenLevok(int x, int y)
        {
            List<JatekElem> PontonLevoElemek=MegadottHelyenLevok(x, y, 0);
            return PontonLevoElemek;
        }

        public void Listaz()
        {
            foreach (JatekElem elem in elemek)
            {
                Console.WriteLine(elem);
            }
        }

        public List<IKirajzolhato> MegjelenítendőElemek()
        {
            List<IKirajzolhato> kirajzolhatok = new List<IKirajzolhato>();
            foreach (JatekElem elem in elemek)
            {
                if (elem is IKirajzolhato)
                {
                    kirajzolhatok.Add(elem as IKirajzolhato);
                }
            }
            return kirajzolhatok;
        }

    }


    class Program
    {
        static void Main(string[] args)
        {
            //1.rész
            /*JatekTer ter = new JatekTer(10,10);
            JatekElem elem1 = new JatekElem(0,0,ter);
            JatekElem elem2 = new JatekElem(5, 5, ter);
            Console.WriteLine("Eredeti:");
            ter.Listaz();
            Console.WriteLine("Kitöröltem");
            ter.Torles(elem1);
            ter.Listaz();
            Console.WriteLine("Felvettem:");
            ter.Felvetel(elem1);
            ter.Listaz();

            Console.WriteLine("Adott(5,5) koordinátájú elem:");
            List<JatekElem> elemek1=ter.MegadottHelyenLevok(5, 5);
            foreach (JatekElem elem in elemek1)
            {
                Console.WriteLine(elem);
            }

            Console.WriteLine("Távolságon belüli elemek:");
            List<JatekElem> elemek2 = ter.MegadottHelyenLevok(5, 5,6);
            foreach (JatekElem elem in elemek2)
            {
                Console.WriteLine(elem);
            }*/



            //2.rész
            Keret.Keret keret = new Keret.Keret();
            keret.Futtatas();
            //Console.ReadKey();
        }
    }
}
