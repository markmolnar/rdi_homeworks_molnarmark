﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Jatek.Jatekter;
using RogueGame.Jatek.Automatizmus;

namespace RogueGame.Jatek.Szabalyok
{
    class GepiJatekos : Jatekos,IAutomatikusanMukodo
    {
        static Random rand;
        public GepiJatekos(string nev, int x, int y, JatekTer ter) : base(nev, x, y, ter)
        {
            rand = new Random();
        }

        public void Mozgas()
        {
            int randomSzam = rand.Next(0,4);
            List<int> probaltSzamok = new List<int>();
            while (true)
            {
                try
                {
                    if (randomSzam == 0)
                    {
                        this.Megy(0, -1);
                        break;
                    }
                    if (randomSzam == 1)
                    {
                        this.Megy(1, 0);
                        break;
                    }
                    if (randomSzam == 2)
                    {
                        this.Megy(0, 1);
                        break;
                    }
                    if (randomSzam == 3)
                    {
                        this.Megy(-1, 0);
                        break;
                    }
                }
                catch (MozgasHelyHianyMiattNemSikerultKivetel e)
                {
                    probaltSzamok.Add(randomSzam);
                    randomSzam = rand.Next(0, 4);
                    while (probaltSzamok.Contains(randomSzam))
                    {
                        randomSzam = rand.Next(0, 4);
                    }
                }
            }
        }

        public void Mukodik()
        {
            Mozgas();
        }

        public override char Alak
        {
            get
            {
                return '\u2640';
            }
        }

        public int MukodesIntervallum
        {
            get
            {
                return 2;
            }
        }
    }
    class GonoszGepiJatekos : GepiJatekos
    {
        public GonoszGepiJatekos(string nev, int x, int y, JatekTer ter) : base(nev, x, y, ter)
        {
        }

        public override char Alak
        {
            get
            {
                return '\u2642';
            }
        }
        public override void  Utkozes(JatekElem elem)
        {
            base.Utkozes(this);
            if (elem is Jatekos && (elem as Jatekos).Aktiv)
            {
                (elem as Jatekos).Serul(10);
            }
        }
    }
}
