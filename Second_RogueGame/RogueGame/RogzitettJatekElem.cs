﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RogueGame.Jatek.Jatekter
{
    abstract class RogzitettJatekElem : JatekElem
    {
        public RogzitettJatekElem(int x, int y, JatekTer ter) : base(x, y, ter)
        {
        }
    }
}
