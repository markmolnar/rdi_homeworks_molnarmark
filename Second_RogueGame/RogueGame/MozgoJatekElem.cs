﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Jatek.Szabalyok;

namespace RogueGame.Jatek.Jatekter
{
    abstract class MozgoJatekElem : JatekElem
    {
        bool aktiv;

        public bool Aktiv
        {
            get
            {
                return aktiv;
            }
            set
            {
                aktiv = value;
            }
        }

        public MozgoJatekElem(int x, int y, JatekTer ter) : base(x, y, ter)
        {
            this.Aktiv = true;
        }

        public void Athelyez(int ujx,int ujy)
        {
            List<JatekElem> ujHelyenLevok = new List<JatekElem>();
            ujHelyenLevok = this.ter.MegadottHelyenLevok(ujx, ujy);
            double sum = 0.0;
            foreach (JatekElem elem in ujHelyenLevok)
            {
                elem.Utkozes(this);
                this.Utkozes(elem);
                if (!Aktiv)
                {
                    throw new MozgasHalalMiattNemSikerultKivetel(this,X,Y);
                }
                else
                {
                    ujHelyenLevok = this.ter.MegadottHelyenLevok(ujx, ujy);
                    foreach (JatekElem e in ujHelyenLevok)
                    {
                        sum += e.Meret;
                    }
                    sum += this.Meret;
                }
            }
            if (sum <= 1)
            {
                this.X = ujx;
                this.Y = ujy;
            }
            else
            {
                throw new MozgasHelyHianyMiattNemSikerultKivetel(this,X,Y,ujHelyenLevok);
            }
        }
    }
}
