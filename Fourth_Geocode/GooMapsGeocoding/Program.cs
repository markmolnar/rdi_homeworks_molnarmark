﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GooMapsGeocoding
{
    class Program
    {
        static SemaphoreSlim sem = new SemaphoreSlim(3);
        static Object lockobject = new Object();
        static void Main(string[] args)
        {
            //List<string> places = File.ReadAllLines("famousplaces.txt").ToList();
            var textFromFile = (new WebClient() { Encoding = System.Text.Encoding.UTF8 }).DownloadString("http://www.szabozs.hu/_DEBRECEN/kerteszg/famousplaces.txt");
            List<string> places = textFromFile.Split(new[] { Environment.NewLine }, StringSplitOptions.None).ToList();
            List<Task<FamousPlaceGeo>> ts = new List<Task<FamousPlaceGeo>>();
            foreach (string item in places)
            {
                ts.Add(Task.Run(() => Feldolgozas(item)));
            }

            Task.WhenAll(ts.ToArray()).ContinueWith(
                xs =>
                {
                    Console.WriteLine("\nOk lekérdések száma:"+xs.Result.Where(x => x.Status == "OK").Count());
                    Console.Write("Melyik városban vagy most(ékezet nélkül)? -> ");

                    string place = Console.ReadLine();
                    while (!Regex.IsMatch(place, @"^[a-zA-Z]+$"))
                    {
                        Console.Write("Melyik városban vagy most(ékezet nélkül)? -> ");
                        place = Console.ReadLine();
                    }

                    FamousPlaceGeo myplace = new FamousPlaceGeo();
                    myplace=Feldolgozas(place);
                    int counter = 0;
                    while (myplace.Status != "OK")
                    {
                        if (counter == 3)
                        {
                            break;
                        }
                        myplace = Feldolgozas(place);
                        Thread.Sleep(500);
                        counter++;
                    }
                    //1,
                    Console.WriteLine("----Legközelebbi látványosságok:------");
                    foreach (var item in xs.Result.Where(x=>x.Status=="OK").OrderBy(x => distanceInKmBetweenEarthCoordinates(x.Lattitude, x.Longitude, myplace.Lattitude, myplace.Longitude)).Take(10))
                    {
                        Console.WriteLine(item.Name+":"+item.Country);
                    };
                    //2,
                    Console.WriteLine("----Legtöbb látványosságot tartalmazó ország:-----");
                    var q2 = from x in xs.Result
                             group x by x.Country into g
                             select new
                             {
                                 Nev = g.Key,
                                 Darab = g.Count()
                             };
                    foreach (var country in q2.Where(x=>x.Nev != "").OrderByDescending(x=>x.Darab).Take(1))
                    {
                        List<FamousPlaceGeo> list = xs.Result.Where(x => x.Country == country.Nev).ToList();
                        Console.WriteLine(country.Nev+":"+country.Darab+":");
                        foreach (FamousPlaceGeo p in list)
                        {
                            Console.WriteLine("-"+p.Name);
                        }
                    }
                    //3,
                    Console.WriteLine("----Egymáshoz Legközelebb levő helyek:-----");
                    List<FamousPlaceGeo> allPlaces = xs.Result.Where(x=>x.Status == "OK").ToList();
                    double mindist = 10000;
                    string[] nearPlaces= new string[2];
                    for(int i = 0; i < allPlaces.Count - 1; i++)
                    {
                        for (int j = i + 1; j < allPlaces.Count; j++)
                        {
                            if (distanceInKmBetweenEarthCoordinates(allPlaces[i].Lattitude, allPlaces[i].Longitude, allPlaces[j].Lattitude, allPlaces[j].Longitude) < mindist)
                            {
                                mindist = distanceInKmBetweenEarthCoordinates(allPlaces[i].Lattitude, allPlaces[i].Longitude, allPlaces[j].Lattitude, allPlaces[j].Longitude);
                                nearPlaces[0]=allPlaces[i].Name;
                                nearPlaces[1]=allPlaces[j].Name;
                            }
                        }
                    }
                    Console.WriteLine(mindist+"km "+":"+nearPlaces[0]+"-"+nearPlaces[1]);

                    //4,
                    Console.WriteLine("----Legtávolabbi egy országban lévő helyek-----");
                    var q4 = from x in xs.Result
                             where x.Status=="OK" && x.Country != ""
                             group x by x.Country into g
                             select new
                             {
                                 Orszag = g.Key,
                                 Darab = g.Count()
                             };
                    var q4b = from x in q4
                              where (x.Darab > 1)
                              select (x.Orszag);

                    double maxDist = 0;
                    string[] distantPlaces = new string[2];
                    foreach (string minTwoPlaceCountry in q4b)
                    {
                        List<FamousPlaceGeo> l = xs.Result.Where(x => x.Country == minTwoPlaceCountry).ToList();
                        for (int i = 0; i < l.Count-1; i++)
                        {
                            for (int j = i + 1; j < l.Count(); j++)
                            {
                                if (distanceInKmBetweenEarthCoordinates(l[i].Lattitude, l[i].Longitude, l[j].Lattitude, l[j].Longitude) > maxDist)
                                {
                                    maxDist = distanceInKmBetweenEarthCoordinates(l[i].Lattitude, l[i].Longitude, l[j].Lattitude, l[j].Longitude);
                                    distantPlaces[0] = l[i].Name;
                                    distantPlaces[1] = l[j].Name;
                                }
                            }
                        }
                    }
                    Console.WriteLine(maxDist + "km " + ":" + distantPlaces[0] + "-" + distantPlaces[1]);

                });
                Console.ReadKey();
        }
        static FamousPlaceGeo Feldolgozas(string place)
        {
            sem.Wait();
            Console.WriteLine("{0} letöltése megkezdve", place);
            Thread.Sleep(500);
            XDocument xdoc = XDocument.Load(getUrl(place));
            Console.WriteLine("{0} letöltve", place);
            sem.Release();
            lock (lockobject)
            {
                var q = (from x in xdoc.Descendants("GeocodeResponse")
                         let result = x.Element("result")
                         let countryelement = x.Descendants("type").Where(x => x.Value.Contains("country")).Count()
                         select new FamousPlaceGeo
                         {
                             Status = x.Element("status").Value,
                             Name = place,
                             Country = (result == null || countryelement == 0)? "":x.Descendants("type").Where(x => x.Value.Contains("country")).First().Parent.Element("long_name").Value,
                             Address = result == null ? "" : x.Element("result").Element("formatted_address").Value,
                             Lattitude = result == null ? 0 : double.Parse(x.Element("result").Element("geometry").Element("location").Element("lat").Value.Replace('.',',')),
                             Longitude = result == null ? 0 : double.Parse(x.Element("result").Element("geometry").Element("location").Element("lng").Value.Replace('.',','))
                         });
                Console.WriteLine(place + " feldolgozas veget ert.");
                return q.First();
            }
        }

        static string getUrl(string name)
        {
            return string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address={0}",name);
        }

        static double distanceInKmBetweenEarthCoordinates(double lat1, double lon1, double lat2, double lon2)
        {
            var earthRadiusKm = 6371;

            var dLat = ConvertToRadians(lat2 - lat1);
            var dLon = ConvertToRadians(lon2 - lon1);

            lat1 = ConvertToRadians(lat1);
            lat2 = ConvertToRadians(lat2);

            var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                    Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            return earthRadiusKm * c;


        }

        static double ConvertToRadians(double angle)
        {
            return (Math.PI / 180) * angle;
        }
    }



    class FamousPlaceGeo
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }
        public string Country { get; set; }
        public double Lattitude { get; set; }
        public double Longitude { get; set; }
    }
}
