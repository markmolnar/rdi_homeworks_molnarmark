﻿using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.DTO;

namespace Web.Controllers
{
    public class MovieController : ApiController
    {
        ILogic logic;
        public MovieController()
        {
            logic = new RealLogic();
        }

        [HttpGet]
        [ActionName("all")]
        public IEnumerable<MovieDTO> GetAllMovies()
        {
            List<Movie> movieList = logic.getMovies();
            return MovieDTO.Mapper.Map<List<Movie>, IEnumerable<MovieDTO>>(movieList);
        }

        [HttpPost]
        [ActionName("add")]
        public int AddMovie(MovieDTO movie)
        {
            Movie mov = MovieDTO.Mapper.Map<MovieDTO,Movie>(movie);
            logic.AddMovie(mov);
            return mov.MOVIE_ID;
        }

        [HttpGet]
        [ActionName("del")]
        public int DelMovie(int id)
        {
            logic.DelMovie(id);
            return 1;
        }

        [HttpGet]
        [ActionName("averageimdb")]
        public decimal AvgIMDBScore()
        {
            return logic.GetAverageIMDBScore();
        }

        [HttpPost]
        [ActionName("modify")]
        public int ModifyMovie(MovieDTO movie)
        {
            Movie mov = MovieDTO.Mapper.Map<MovieDTO, Movie>(movie);
            logic.ModifyMovie(mov);
            return mov.MOVIE_ID;
        }
    }
}
