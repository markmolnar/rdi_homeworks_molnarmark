﻿using AutoMapper;
using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.DTO;

namespace Web
{
    public class WebMapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MovieDTO, Movie>().ReverseMap();
            });
            return config.CreateMapper();
        }
    }
}