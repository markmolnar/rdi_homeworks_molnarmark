﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using Data;
using Repo.MovieRepos;
using Repo.CategoryRepos;
using System;

namespace Logic
{
    public class RealLogic : ILogic
    {
        MovieEFRepository movieRepo;
        IMapper mapper;
        public RealLogic()
        {
            MovieEntities ME = new MovieEntities();
            movieRepo = new MovieEFRepository(ME);
            //repo = new MyRepository(empRepo, deptRepo);
            mapper = MapperFactory.CreateMapper();
        }
        public void AddMovie(Movie newmovie)
        {
            MOVIE mov = mapper.Map<Movie, MOVIE>(newmovie);
            movieRepo.Insert(mov);
        }

        public void DelMovie(int movieID)
        {
            movieRepo.Delete(movieID);
        }
    
        public List<Movie> getMovies()
        {
            IQueryable<MOVIE> list = movieRepo.GetAll();
            return mapper.Map<IQueryable<MOVIE>, List<Movie>>(list);
        }

        public string MostProductiveYear()
        {
            var q = from x in movieRepo.GetAll()
                    group x by x.RELEASE_YEAR into g
                    select new
                    {
                        Year = g.Key,
                        Count = g.Count()
                    };
            var q2 = q.OrderByDescending(x => x.Count).Take(1).Select(x => x.Year).ToString();
            return q2;        
        }

        public decimal GetAverageIMDBScore()
        {
            var q = movieRepo.GetAll().Average(x => x.IMDB_SCORE ?? 0);
            return q;
        }

        public void ModifyMovie(Movie movie)
        {
            movieRepo.Modify(movie.MOVIE_ID, movie.IMDB_SCORE ?? 0);
        }
    }
}
