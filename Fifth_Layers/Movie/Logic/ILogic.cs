﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public interface ILogic
    {
        decimal GetAverageIMDBScore();
        List<Movie> getMovies();
        void DelMovie(int movieID);
        void AddMovie(Movie movie);
        string MostProductiveYear();
        void ModifyMovie(Movie movie);
    }
}
