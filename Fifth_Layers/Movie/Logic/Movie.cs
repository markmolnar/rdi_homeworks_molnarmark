﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class Movie
    {
        public int MOVIE_ID { get; set; }
        public string TITLE { get; set; }
        public string RELEASE_YEAR { get; set; }
        public Nullable<decimal> IMDB_SCORE { get; set; }
        public Nullable<short> LENGTH { get; set; }
        public byte CATEGORY_ID { get; set; }
    }
}
