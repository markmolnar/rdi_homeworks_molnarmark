﻿using Data;
using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie
{
    class Program
    {
        static void Main(string[] args)
        {
            MovieEntities ME = new MovieEntities();
            ME.Database.Log = Console.WriteLine;
            Console.WriteLine(ME.MOVIE.Count());

            var q = from x in ME.MOVIE
                    group x by x.CATEGORY into g
                    select new
                    {
                        Category = g.Key.CATEGORY_NAME,
                        Number = g.Count()
                    };
            foreach (var item in q.OrderByDescending(x=>x.Number))
            {
                Console.WriteLine(item.Category+":"+item.Number);
            }
            CATEGORY dramaCategory = ME.CATEGORY.Where(x => x.CATEGORY_NAME == "Drama").Single();
            MOVIE movie = new MOVIE();
            movie.TITLE = "Life Of Pi";
            movie.RELEASE_YEAR = "2012";
            movie.IMDB_SCORE =(decimal)7.9;
            movie.LENGTH = 127;
            movie.CATEGORY_ID = dramaCategory.CATEGORY_ID;

            ME.MOVIE.Add(movie);
            ME.SaveChanges();
            foreach (var item in ME.MOVIE)
            {
                Console.WriteLine(item.TITLE);
            }
            ME.MOVIE.Remove(ME.MOVIE.Where(x=>x.TITLE == "Life Of Pi").Single());
            ME.SaveChanges();
            foreach (var item in ME.MOVIE)
            {
                Console.WriteLine(item.TITLE);
            }
            Console.ReadKey();
        }
    }
}
