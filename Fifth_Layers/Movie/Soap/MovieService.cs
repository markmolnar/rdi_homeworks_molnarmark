﻿using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Soap
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Single, InstanceContextMode = InstanceContextMode.PerSession)]
    public class MovieService : IMovieService
    {
        ILogic logic;
        public MovieService()
        {
            logic = new RealLogic();
        }
        public bool AddMovie(MovieDTO movie)
        {
            Movie m = MovieDTO.Mapper.Map<MovieDTO, Movie>(movie);
            logic.AddMovie(m);
            return true;
        }

        public bool DelMovie(int movieID)
        {
            logic.DelMovie(movieID);
            return true;
        }

        public decimal GetAverageIMDBScore()
        {
            return logic.GetAverageIMDBScore();
        }

        public List<MovieDTO> GetMovies()
        {
            List<Movie> movies = logic.getMovies();
            return MovieDTO.Mapper.Map<List<Movie>, List<MovieDTO>>(movies);
        }

        public string MostProductiveYear()
        {
            return logic.MostProductiveYear();
        }
    }
}
