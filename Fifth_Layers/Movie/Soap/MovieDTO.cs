﻿using AutoMapper;
using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Soap
{
    [DataContract]
    public class MovieDTO
    {
        public static IMapper Mapper { get; private set; }
        static MovieDTO()
        {
            Mapper = SoapMapperFactory.CreateMapper();
        }
        [DataMember]
        public int MOVIE_ID { get; set; }
        [DataMember]
        public string TITLE { get; set; }
        [DataMember]
        public string RELEASE_YEAR { get; set; }
        [DataMember]
        public Nullable<decimal> IMDB_SCORE { get; set; }
        [DataMember]
        public Nullable<short> LENGTH { get; set; }
        [DataMember]
        public byte CATEGORY_ID { get; set; }
    }
}
