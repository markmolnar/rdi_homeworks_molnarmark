﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Soap
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IMovieService
    {
        [OperationContract]
        bool AddMovie(MovieDTO movie);

        [OperationContract]
        bool DelMovie(int movieID);

        [OperationContract]
        List<MovieDTO> GetMovies();

        [OperationContract]
        decimal GetAverageIMDBScore();

        [OperationContract]
        string MostProductiveYear();

        // TODO: Add your service operations here
    }
}
