﻿using Data;
using Repo.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repo.CategoryRepos
{
    public class CategoryEFRepository : EFRepository<CATEGORY>, ICategoryRepository
    {
        public CategoryEFRepository(DbContext ctx) : base(ctx)
        {
        }

        public override CATEGORY GetByID(int id)
        {
            return context.Set<CATEGORY>().Where(x => x.CATEGORY_ID == (byte)id).Single();
        }
    }
}
