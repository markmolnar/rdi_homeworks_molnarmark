﻿using Data;
using Repo.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repo.MovieRepos
{
    public class MovieEFRepository : EFRepository<MOVIE>, IMovieRepository
    {
        public MovieEFRepository(DbContext ctx) : base(ctx)
        {
        }

        public override MOVIE GetByID(int id)
        {
            return context.Set<MOVIE>().Where(x => x.MOVIE_ID == id).Single();
        }

        public void Modify(int id,decimal newIMDBScore)
        {
            MOVIE movie = GetByID(id);
            if (movie != null)
            {
                movie.IMDB_SCORE = newIMDBScore;
            }
            else
            {
                throw new ArgumentException("No Data");
            }
            context.SaveChanges();
        }
    }
}
