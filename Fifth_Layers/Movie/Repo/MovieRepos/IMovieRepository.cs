﻿using Data;
using Repo.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo.MovieRepos
{
    interface IMovieRepository:IRepository<MOVIE>
    {
        void Modify(int id,decimal newIMDBScore);
    }
}
