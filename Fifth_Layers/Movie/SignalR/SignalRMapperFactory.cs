﻿using AutoMapper;
using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalR
{
    class SignalRMapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MovieDTO, Movie>().ReverseMap();
            });
            return config.CreateMapper();
        }
    }
}
