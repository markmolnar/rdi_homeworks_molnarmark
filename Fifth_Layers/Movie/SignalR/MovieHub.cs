﻿using Logic;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalR
{
   public class MovieHub:Hub
    {
        ILogic logic;
        public MovieHub()
        {
            logic = new RealLogic();
        }
        public void AddMovie(MovieDTO movie)
        {
            Movie m = MovieDTO.Mapper.Map<MovieDTO, Movie>(movie);
            logic.AddMovie(m);
            Clients.All.NewDept(m);
        }

        public void DelMovie(int movieID)
        {
            logic.DelMovie(movieID);
            Clients.All.Refresh();
        }

        public decimal GetAverageIMDBScore()
        {
            return logic.GetAverageIMDBScore();
        }

        public List<MovieDTO> GetMovies()
        {
            List<Movie> movies = logic.getMovies();
            return MovieDTO.Mapper.Map<List<Movie>, List<MovieDTO>>(movies);
        }

        public string MostProductiveYear()
        {
            return logic.MostProductiveYear();
        }
    }
}
