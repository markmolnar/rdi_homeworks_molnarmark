﻿using AutoMapper;
using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalR
{
    public class MovieDTO
    {
        public static IMapper Mapper { get; private set; }
        static MovieDTO()
        {
            Mapper = SignalRMapperFactory.CreateMapper();
        }
        public int MOVIE_ID { get; set; }
        public string TITLE { get; set; }
        public string RELEASE_YEAR { get; set; }
        public Nullable<decimal> IMDB_SCORE { get; set; }
        public Nullable<short> LENGTH { get; set; }
        public byte CATEGORY_ID { get; set; }
    }
}
