﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_SignalRClient
{
    public class Mov
    {
        public int MOVIE_ID { get; set; }
        public string TITLE { get; set; }
        public string RELEASE_YEAR { get; set; }
        public Nullable<decimal> IMDB_SCORE { get; set; }
        public Nullable<short> LENGTH { get; set; }
        public byte CATEGORY_ID { get; set; }
        public override string ToString()
        {
            return $"#{TITLE} {RELEASE_YEAR}";
        }
    }

    class Program
    {
        static async void RefreshList(IHubProxy signalr)
        {
            Console.WriteLine("FETCHING FROM SIGNALS");
            var list = await signalr.
            Invoke<IEnumerable<Mov>>("GetMovies");
            foreach (var item in list)
            {
                Console.WriteLine(item.ToString());
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("PRESS ENTER");
            Console.ReadKey();

            string url = "http://localhost:8040";
            var hubConnection = new HubConnection(url);
            var hubProxy = hubConnection.CreateHubProxy("MovieHub");
            hubProxy.On("Refresh", () => {
                Console.WriteLine("Refesh call from server");
                RefreshList(hubProxy);

            });
            hubProxy.On<Mov>("NewMovie", param =>
            {
                Console.WriteLine("NEW MOVIE FROM SERVER" + param);
            });
            hubConnection.Start().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine("ERROR" + task.Exception.GetBaseException());
                }
                else
                {
                    Console.WriteLine("CONNECTED");
                }
            }).Wait();
            Console.ReadLine();
            RefreshList(hubProxy);
            Console.ReadLine();

            Console.WriteLine("Adding");
            Mov m= new Mov() { MOVIE_ID=1090,TITLE="Batman Begins",IMDB_SCORE=(decimal)8.3,LENGTH=140,RELEASE_YEAR="2005",CATEGORY_ID=50};
            hubProxy.Invoke("AddMovie", m).Wait();
            Console.WriteLine("ADDED");
            Console.ReadLine();

            RefreshList(hubProxy);
            Console.ReadLine();

            Console.WriteLine("AverageIMDBScore:");
            var avg = hubProxy.Invoke<decimal>("GetAverageIMDBScore").Result;
            Console.Write(avg);
            Console.ReadLine();

            Console.WriteLine("Deleting");
            hubProxy.Invoke("DelMovie", m.MOVIE_ID).Wait();
            Console.WriteLine("DELETED");
            Console.ReadLine();

            Console.WriteLine("AverageIMDBScore:");
            avg=hubProxy.Invoke<decimal>("GetAverageIMDBScore").Result;
            Console.Write(avg);
            Console.ReadLine();


            hubConnection.Stop();
            Console.WriteLine("STOPPPED");
            Console.ReadLine();
        }
    }
}
