﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_WebClient
{
    class Mov
    {
        public int MOVIE_ID { get; set; }
        public string TITLE { get; set; }
        public string RELEASE_YEAR { get; set; }
        public Nullable<decimal> IMDB_SCORE { get; set; }
        public Nullable<short> LENGTH { get; set; }
        public byte CATEGORY_ID { get; set; }
        public override string ToString()
        {
            return $"#{MOVIE_ID}: {TITLE} @ {RELEASE_YEAR}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:55537/api/movie/";
            Console.WriteLine("Waiting...");
            Console.ReadKey();

            using (WebClient client = new WebClient())
            {
                string json = client.DownloadString(url + "all");
                var list = JsonConvert.DeserializeObject<List<Mov>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item.ToString());
                }

                NameValueCollection postData;
                byte[] responseBytes;
                postData = new NameValueCollection();
                postData.Add("movie_id", "1070");
                postData.Add("title", "Batman Begins");
                postData.Add("imdb_score", "8.2");
                postData.Add("release_year","2005");
                postData.Add("length","140");
                postData.Add("category_id","50");
                responseBytes = client.UploadValues(url + "add", "POST", postData);
                Console.WriteLine("ADD", Encoding.UTF8.GetString(responseBytes));
                Console.WriteLine("ALL: ", client.DownloadString(url + "all"));
                postData = new NameValueCollection();
                postData.Add("movie_id", "1070");
                postData.Add("title", "Batman Begins");
                postData.Add("imdb_score", "8.3");
                postData.Add("release_year", "2005");
                postData.Add("length", "140");
                postData.Add("category_id", "50");
                responseBytes = client.UploadValues(url + "modify", "POST", postData);
                Console.WriteLine("MOD:", Encoding.UTF8.GetString(responseBytes));
                Console.WriteLine("ALL: " + client.DownloadString(url + "all"));
                Console.WriteLine("AvgIMDB: " + client.DownloadString(url + "averageimdb"));
                Console.WriteLine("DEL: " + client.DownloadString(url + "del/1070"));
                Console.WriteLine("ALL: " + client.DownloadString(url + "all"));
                Console.WriteLine("AvgIMDB: "+client.DownloadString(url+"averageimdb"));
                Console.WriteLine("All done");
                Console.ReadKey();
            }
        }
    }
}
