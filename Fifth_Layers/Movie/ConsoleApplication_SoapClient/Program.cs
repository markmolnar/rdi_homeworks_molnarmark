﻿using ConsoleApplication_SoapClient.MovieSoapService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication_SoapClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("RETURN...");
            Console.ReadLine();
            MovieServiceClient client = new MovieServiceClient();
            foreach (MovieDTO dto in client.GetMovies())
            {
                Console.WriteLine(dto.MOVIE_ID+":"+dto.TITLE);
            }
            Console.WriteLine("...");
            Console.WriteLine("ADDING");
            MovieDTO movie = new MovieDTO() { MOVIE_ID = 1015, TITLE = "Life of PI", RELEASE_YEAR = "2012", IMDB_SCORE = (decimal)7.9,LENGTH=127, CATEGORY_ID=10};
            client.AddMovie(movie);
            foreach (var item in client.GetMovies())
            {
                Console.WriteLine(item.MOVIE_ID+":"+item.TITLE);
            }

            Console.WriteLine("Average IMDB Score:");
            var avg=client.GetAverageIMDBScore();
            Console.Write(avg);
            Console.WriteLine("...");
            Console.WriteLine("DELETING");
            client.DelMovie(movie.MOVIE_ID);
            foreach (var item in client.GetMovies())
            {
                Console.WriteLine(item.MOVIE_ID+":"+item.TITLE);
            }

            Console.WriteLine("Average IMDB Score:");
            avg = client.GetAverageIMDBScore();
            Console.Write(avg);
            Console.ReadKey();
        }
    }
}
